## [1.0.3](https://gitlab.com/agungkes/puppeteer-extra-plugin-screenshot/compare/v1.0.2...v1.0.3) (2021-06-26)


### Bug Fixes

* increase font size and bold ([168f92f](https://gitlab.com/agungkes/puppeteer-extra-plugin-screenshot/commit/168f92ff84bc9a2b3f5bc392a7a26c4557b41db9))

## [1.0.2](https://gitlab.com/agungkes/puppeteer-extra-plugin-screenshot/compare/v1.0.1...v1.0.2) (2021-06-22)


### Bug Fixes

* change zindex to 9999 ([3677782](https://gitlab.com/agungkes/puppeteer-extra-plugin-screenshot/commit/36777826db3d8e10e9b92534fddb2c593bd3896e))

## [1.0.1](https://gitlab.com/agungkes/puppeteer-extra-plugin-screenshot/compare/v1.0.0...v1.0.1) (2021-06-20)


### Bug Fixes

* give commented on class ([3c1ce9d](https://gitlab.com/agungkes/puppeteer-extra-plugin-screenshot/commit/3c1ce9d4f7ae66914d995ead7f1215d8373dc485))
* remove test on ci ([4293bd9](https://gitlab.com/agungkes/puppeteer-extra-plugin-screenshot/commit/4293bd98cf69c10c48412a91e1f2645a3c4f1e92))

## [1.0.1-beta.1](https://gitlab.com/agungkes/puppeteer-extra-plugin-screenshot/compare/v1.0.0...v1.0.1-beta.1) (2021-06-20)


### Bug Fixes

* give commented on class ([3c1ce9d](https://gitlab.com/agungkes/puppeteer-extra-plugin-screenshot/commit/3c1ce9d4f7ae66914d995ead7f1215d8373dc485))

# 1.0.0 (2021-06-14)


### Bug Fixes

* change packagename ([10f42c7](https://gitlab.com/agungkes/puppeteer-extra-screenshot/commit/10f42c709c89efdbae991be11f46ad445fb4a2f3))
* **release:** add npm step ([c9d2faf](https://gitlab.com/agungkes/puppeteer-extra-screenshot/commit/c9d2fafa23f8e9bbfef00431e59485d68e111270))
