module.exports = {
  preset: './jest.preset.js',
  rootDir: '.',
  // testEnvironment: 'node',
  roots: ['<rootDir>/src'],
  testRegex: '.(e2e-)?\\.(spec|test)\\.(ts|js)$',
  transform: {
    '^.+\\.ts$': 'ts-jest',
  },
  moduleFileExtensions: ['js', 'json', 'ts'],
  collectCoverageFrom: [
    'src/**/*.ts',
    'src/**/*.tsx',
    '!src/**/*.(e2e-)?\\.(spec|test)\\.ts',
    '!src/**/*.(e2e-)?\\.(spec|test)\\.tsx',
  ],
  // https://github.com/istanbuljs/istanbuljs/tree/master/packages/istanbul-reports/lib
  coverageReporters: ['text-summary', 'html', 'lcovonly'],
  globalSetup: 'jest-environment-puppeteer/setup',
  globalTeardown: 'jest-environment-puppeteer/teardown',
  testEnvironment: 'jest-environment-puppeteer',
};
