# puppeteer-extra-plugin-screenshot

> A plugin for [puppeteer-extra](https://github.com/berstend/puppeteer-extra) to take screenshot and add watermark.

[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release) [![License: MIT](https://img.shields.io/badge/License-MIT-green.svg)](https://gitlab.com/agungkes/puppeteer-extra-plugin-screenshot/-/raw/main/LICENSE)
[![Code Style: Google](https://img.shields.io/badge/code%20style-google-blueviolet.svg)](https://github.com/google/gts)

## Install

```sh
npm install puppeteer-extra-plugin-screenshot
# or
yarn add puppeteer-extra-plugin-screenshot
```

If this is your first [puppeteer-extra](https://github.com/berstend/puppeteer-extra) plugin here's everything you need:

```sh
yarn add puppeteer puppeteer-extra puppeteer-extra-plugin-screenshot
# or
npm install puppeteer puppeteer-extra puppeteer-extra-plugin-screenshot
```

## How to use

```js
const puppeteer = require('puppeteer-extra');
const screenshotPlugin = require('puppeteer-extra-plugin-screenshot');

puppeteer.use(screenshotPlugin());

async function getPage(url) {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.goto(url);
  const image = await page.takeScreenshot();

  await browser.close();
  return image;
}
```

## Development

1. Clone this repository
2. Run command `yarn install` (yarn v2 or later) or `npm install`
3. That's it!
