import { PuppeteerExtraPlugin } from 'puppeteer-extra-plugin';
// eslint-disable-next-line
// @ts-ignore
import type { Page } from 'puppeteer';

export type ScreenshotPage = {
  takeScreenshot: (text: string) => Promise<string>;
};

/**
 * Screenshot Plugin with Watermark
 */
class ScreenshotPlugin extends PuppeteerExtraPlugin {
  constructor(config = {}) {
    super(config);
  }

  get name() {
    return 'screenshot-extra';
  }

  async takeScreenshot(watermarkText: string): Promise<string> {
    const runTakeScreenshot = (text: string) => {
      const bodyElement = document.querySelector('body');
      const captionElement = document.createElement('H2');
      captionElement.setAttribute(
        'style',
        'padding: 5px 30px; position: fixed; top: 0; left: 0; right: 0; background-color: white; z-index: 9999; font-size: 18px; font-weight: bold'
      );

      const t = document.createTextNode(text);
      captionElement.appendChild(t);

      if (bodyElement) {
        bodyElement.insertBefore(captionElement, bodyElement.children[1]);
        window.scrollTo(0, 0);
      }
    };

    /* eslint-disable */
    // @ts-ignore
    await this.evaluate(runTakeScreenshot, watermarkText);

    // @ts-ignore
    const image = await this.screenshot({
      fullPage: true,
      encoding: 'base64',
    });

    /* eslint-enable */
    return image;
  }

  async onPageCreated(page: Page & ScreenshotPage) {
    page.takeScreenshot = this.takeScreenshot.bind(page);
  }
}

const defaultExport = (opts?: Record<string, unknown>) =>
  new ScreenshotPlugin(opts);
export default defaultExport;
