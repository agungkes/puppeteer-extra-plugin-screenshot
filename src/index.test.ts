jest.setTimeout(35000);
const PLUGIN_NAME = 'screenshot-extra';
import Plugin from '.';

describe('Puppeteer Extra Plugin Screenshot', () => {
  it('should a function', () => {
    expect(typeof Plugin).toBe('function');
  });

  it('should have the basic class members', () => {
    const instance = Plugin();
    expect(instance.name).toMatch(PLUGIN_NAME);
    expect(instance._isPuppeteerExtraPlugin).toBeTruthy();
  });

  it('should have the public child class members', () => {
    const instance = Plugin();
    const prototype = Object.getPrototypeOf(instance);
    const childClassMembers = Object.getOwnPropertyNames(prototype);

    expect(childClassMembers.includes('constructor')).toBeTruthy();
    expect(childClassMembers.includes('name')).toBeTruthy();
    expect(childClassMembers.includes('onPageCreated')).toBeTruthy();
  });

  it('should take screenshot of a page and add watermark on top', async () => {
    const instance = Plugin();
    expect(instance.takeScreenshot('ini watermark')).toBeTruthy();

    // await page.goto('https://google.com', {
    //   waitUntil: 'networkidle2',
    // });

    // // eslint-disable-next-line
    // // @ts-ignore
    // const image = await page.takeScreenshot('ini watermark');

    // expect(typeof image).toBe('string');
  });
});
